<?php
/*
    In the beginning, there was nothing,
    the screen was dark.

    Dev grew bored with this darkness
    Before long Dev decided to create

    On the first day Dev created the databse
    with it, the tables, and columns too

    On the second day, Dev created the config
    within the config were the GLOBALS and KEYS

    On the third day, Dev created the classes
    each class needed much love and attention.

    On the fourth day, Dev created the methods
    expanding the classes meant better systems

    And on the fifth day, Dev rested for he was done
    These things came together to create the backend
    upon which everything is built.

    Remember to thank your Dev
    and buy him a beer
 */

/**
 *  Drive CRM
 *  @author Bret Wagner <bwagner@drivestl.com>
 *  @version 0.1.0
 *
 */
session_start();

// db connection properties
define('DBHOST', 'localhost');
define('DBNAME', 'drivestl_crm');
define('DBUSER', 'drivestl_admin');
define('DBPASS', '6X]qA%6D;w_1');

// Create key for encryption
define('KEY', pack('H*', bin2hex(openssl_random_pseudo_bytes(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)))));
// define('KEY', 'P50448B03NssnM@xGleDrive');

// Define site path
define('DIR', 'http://drivestltest.com/crm');
// Define admin path
define('DIRADMIN', DIR . "/admin");
// Define site title
define('SITETITLE', 'Drive CRM');

// Define include checker
define('included', 1);
include('functions.php');
